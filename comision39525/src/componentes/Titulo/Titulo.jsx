const Titulo = ( { titulo, subTitulo } ) => { // component presentación
      
    return (
        <div>
            <h2>{titulo}</h2>
            <h4>{subTitulo}</h4>            

        </div>        
    )
}


export default Titulo
