
import Titulo from "../Titulo/Titulo"


const HomeContainer = ({compHijo}) => {
    const titulo = 'Titulo app' // estado de app
    const subTituloApp = 'Subtitulo de app' // estado
    const saludar = ()=>{
        console.log('Saludando')
    }

    // console.log(children)
    return (
        <div>
            <Titulo titulo={titulo} subTitulo={subTituloApp} />
                
            {/* {children} */}
            { compHijo({}) }
        </div>
    )
}

export default HomeContainer