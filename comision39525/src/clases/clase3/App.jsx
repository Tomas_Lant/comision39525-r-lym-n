import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)
  console.log('Ejemplos')
  const condition = true
  // let resultado = null 

  // if (condition) {
  //   resultado = 'Verdadero'
  // } else {
  //   resultado = 'Falso'
  // }

  // console.log('El resultado es: ' + resultado)
  // if else -> condition  ? :
  // console.log(` El resultado es: ${ condition ? 'Verdadero' : 'Falso'}`)

  // const array = [ 2,3,4,5 ] // [1,2,3,4,5] - {}
  //  const uno = 1
  // const newArray = [ ...array, uno]
  // const obj = {
  //   uno: 1, 
  //   dos: 2
  // }

  // const obj1 = {
  //   tres: 3
  // }
  // console.log({ ...obj, ...obj1 })

  // _    _____________________________________________

  const campo = 'osandon'

  const objPersona = {
    nombre: 'Federico Augusto',
    apellido: 'Osandon',
    [campo+'_fede'] :'alshjdkhsafdk',
    
  }

  // // obj.nombre = 'En realidad juan es el mejor'

  
  // destructuring

  // let nombre = obj.nombre
  // let apellido = obj.apellido

  // const { nombre = nombre, apellido = apellido }  = objPersona
  // objPersona.dni = 55555
  
  // const { nombre: firstName, apellido: lastName, dni=323232 }  = objPersona
  
  // console.log(dni)

// nvaegadores suma(10, 2) 2+2+2+2+2+2 -> multiplicar(10, 2) 10 * 2
  // multiplicarPollify (10,2 ) 2+2+2+2+2+2+2+2
  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Fede el mejor</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/App.jsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">
        Click on the Vite and React logos to learn more
      </p>
    </div>
  )
}

export default App
